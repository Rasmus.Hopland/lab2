package INF101.lab2;



import java.time.LocalDate;
import java.util.ArrayList;
//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.NoSuchElementException;
import java.util.NoSuchElementException;



public class Fridge implements IFridge {
    //private static final LocalDate Date = LocalDate.of(2021, 12, 24); //LocalDate.now();
    ArrayList<FridgeItem> content = new ArrayList<FridgeItem>();

    public int totalSize() {
        return 20;
    }
    
    /*public static void main(String[] args) {
        IFridge fridge = new Fridge();
        FridgeItem e = new FridgeItem("Milk", Date);
        fridge.placeIn(e);
        fridge.placeIn(e);
        System.out.println(fridge.nItemsInFridge());
        fridge.removeExpiredFood();
        

    }*/

    @Override
    public boolean placeIn(FridgeItem item) {
        //String name = item.getName();
        //LocalDate date = item.getExpirationDate();
        if(nItemsInFridge() == totalSize()) {
            System.out.println("The fridge is full");
            return false;
        } else {
            content.add(item);
            System.out.println("Item was added to the list");
            return true;
        }

    }

    @Override
    public void takeOut(FridgeItem item) {
        //String name = item.getName();
        //LocalDate date = item.getExpirationDate();
        if(nItemsInFridge() == 0) {
            throw new NoSuchElementException("The fridge is empty, no items to remove");
        } else if(!content.contains(item)) {
            System.out.println("No such item in fridge.");
        } else {
            content.remove(item);
        } 
        
    }

    @Override
    public void emptyFridge() {
        content.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<FridgeItem>();
        for (FridgeItem i : content) {
            if (i.hasExpired() == true) {
                expired.add(i);
            }
        }
        for (FridgeItem x : expired) {
            content.remove(x);
        }
        return expired;
    }

    @Override
    public int nItemsInFridge() {
        int temp = content.size();
        return temp;
    }
}
